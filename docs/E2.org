#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:E2 Limpieza y Visualización
#+SUBTITLE: Visualización de la Información
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:julian.perezromero@educa.madrid.org
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="r-fit-text" >%t</h1><br><br><h3 class="subtitle">%s</h3><br><h4>%a</h4><br><p>Máster Diseño Interactivo</p><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Enunciado
- Para este ejercicio tenemos que buscar un dataset o conjunto de datos con el que queramos trabajar
- Una vez encontrado y descargado el dataset, el siguiente paso será abrirlo en Open Refine y realizar los pasos indicados en la slide para limpiarlo u observar patrones
- Una vez limpio y transformados los datos pasaremos a exportarlo en formato CSV para poder importarlo en Datawrapper
- En datawrapper escogeremos la gráfica o mapa que más se ajuste a lo que buscamos comunicar
* A tener en cuenta
- Ejercicio individual
- Pregunta si no sabes dónde encontrar el conjunto de datos que buscas
- Apóyate de la slide 01 donde se muestra la imagen "Qué gráfico escoger?"
- Deja en el dataset únicamente los datos que te interesen visualizar
- Si tenéis cualquier duda de transformación no dudéis en escribirme por Slack
* Puntos a desarrollar
- Limpieza con Open Refine del dataset que escogas
- Visualización con Datawrapper
* Algunas referencias                                              :noexport:
- Documentación sobre el proceso de visualización del dataset de las tarjetas black por Montera34:
  - https://numeroteca.org/2017/01/22/un-experimento-de-small-data-black-to-de-future/
  - https://voragine.net/visualizacion-de-datos/black-to-de-future
* Entrega
- Entrega del dataset limpio y enlace a la visualización en Datawrapper por el campus virtual
- Entrega: *31 marzo*
