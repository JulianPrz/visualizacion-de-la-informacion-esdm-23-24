#+STARTUP: indent
#+STARTUP: overview

:REVEAL_PROPERTIES:
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: simple
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+OPTIONS: timestamp:nil toc:1 num:nil author:nil date:nil
:END:  

#+TITLE:E3 Visualización Interactiva
#+SUBTITLE: Visualización de la Información
#+AUTHOR: Julián Pérez
#+LANGUAGE: es
#+EMAIL:julian.perezromero@educa.madrid.org
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+REVEAL_PLUGINS: (highlight CopyCode)
#+REVEAL_HIGHLIGHT_CSS: https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.4.0/styles/base16/espresso.min.css
#+REVEAL_EXTRA_CSS: ../assets/css/modifications.css
#+REVEAL_EXTRA_CSS: ../assets/fonts/webfont-iosevka-11.3.0/iosevka.css
#+REVEAL_TITLE_SLIDE: <h1 class="r-fit-text" >%t</h1><br><br><h3 class="subtitle">%s</h3><br><h4>%a</h4><br><p>Máster Diseño Interactivo</p><p>Escuela Superior de Diseño de Madrid</p>
#+OPTIONS: toc:nil

* Enunciado
- Esta práctica consiste en hacer una variación del gráfico realizado
  en clase sobre Cambio Climática conocido como Warm Stripes,
  programado en p5js, que sea interactivo
* Warm Stripes
- Se puede realizar con el dataset que trabajamos en clase o con otro
- La variación puede ser tanto en la estética como en la interactividad
- En esta página aparecen diferentes maneras de mostrar la misma información: https://showyourstripes.info/b/globe
* API's                                                            :noexport:
- Podemos trabajar con las mismas API's vistas en clase o con otras que queramos
- La interactividad puede ser el input de un dato a consultar en la
  API. Por ejemplo, hay un campo de entrada donde metemos una ciudad y
  me devuelve los datos meteorológicos y los visualizo a mi estilo
  personal
- Otra interactividad puede ser con un hover sobre los elementos y que ofrezca datos en un tooltip
* A tener en cuenta
- Ejercicio individual
- Buena estructura de los archivos del proyecto
- Comentarios al inicio del código que explique la variación de la
  gráfica o de lo que se quiere conseguir, así como comentarios para
  explicar el funcionamiento del código
* Puntos a desarrollar
- Desarrollo código con p5js
* Evaluación
- Se valorará la ideación, el desarrollo del código, la interactividad
  de la gráfica, la calidad estética, cumplir con los requisitos y la
  entrega a tiempo
* Entrega
- Entrega por el aula virtual del proyecto web que muestre la gráfica interactiva
- Entrega: *19 abril*
