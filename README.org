#+OPTIONS: toc:nil

* Visualización de datos
# URL: https://julianprz.gitlab.io/visualizacion-de-la-informacion-esdm-22-23/main/docs/.html
*** Temas
    1. [[https://julianprz.gitlab.io/visualizacion-de-la-informacion-esdm-22-23/main/docs/01-introduccion.html][Introducción]] (Día 1)
    2. [[https://julianprz.gitlab.io/visualizacion-de-la-informacion-esdm-22-23/main/docs/02-compilacion-datos.html][Compilación de datos]] (Día 2)
    3. [[https://julianprz.gitlab.io/visualizacion-de-la-informacion-esdm-23-24/main/docs/03-limpieza-datos.html][Limpieza de datos]] (Día 3)
    4. Visualización Interactiva (Día 4)
    5. Sistema de información geográfica (Día5)
*** Prácticas
    - [[ https://julianprz.gitlab.io/visualizacion-de-la-informacion-esdm-22-23/main/docs/E1.html][Ejercicio 1 - Dear Data]]
    - [[https://julianprz.gitlab.io/visualizacion-de-la-informacion-esdm-22-23/main/docs/E2.html][Ejercicio 2 - Limpieza datos]]
    - Ejercicio 3 - Visualización Interactiva
